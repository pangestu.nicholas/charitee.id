const menuBtn = document.querySelector(".menu-icon span");
const searchBtn = document.querySelector(".search-icon");
const cancelBtn = document.querySelector(".cancel-icon");
const cancelMenuBtn = document.querySelector(".cancel-icon-menu");
const items = document.querySelector(".nav-items");
const form = document.querySelector("form");
const body = document.querySelector("body");
menuBtn.onclick = ()=>{
    items.classList.add("active");
    menuBtn.classList.add("hide");
    searchBtn.classList.add("hide");
    cancelMenuBtn.classList.add("show");
    body.style.overflow = "hidden";
}
cancelMenuBtn.onclick = ()=>{
    items.classList.remove("active");
    menuBtn.classList.remove("hide");
    searchBtn.classList.remove("hide");
    cancelMenuBtn.classList.remove("show");
    cancelMenuBtn.style.color = "#ffffff";
    form.classList.remove("active");
    body.style.overflow = "auto";
}
cancelBtn.onclick = ()=>{
    items.classList.remove("active");
    searchBtn.classList.remove("hide");
    cancelBtn.classList.remove("show");
    cancelBtn.style.color = "#ffffff";
    form.classList.remove("active");
}
searchBtn.onclick = ()=>{
    form.classList.add("active");
    searchBtn.classList.add("hide");
    cancelBtn.classList.add("show");
}