<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Admin;
use App\Models\Panti;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'email' => 'admin@charitee.id',
            'nama' => 'Admin Charitee',
            'nomor_kontak' => '735986',
            'password' => bcrypt('Admin.charitee4')
        ]);

    }
}
