<?php
use App\Models\Admin;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\PantiController;
use App\Http\Controllers\DonasiUangController;
use App\Http\Controllers\DonasiBarangController;
use App\Http\Controllers\AdminDonasiUangController;
use App\Http\Controllers\AdminDonasiBarangController;
use App\Http\Controllers\AdminPantiController;
use App\Http\Controllers\AdminKategoriKebutuhanController;

Route::get('/linkstorage', function () { $targetFolder = base_path().'/storage/app/public'; $linkFolder = $_SERVER['DOCUMENT_ROOT'].'/storage'; symlink($targetFolder, $linkFolder); });

Route::get('/',[LoginController::class,'home']);

Route::get('/test-mail',[LoginController::class,'home']);

Route::get('/login',[LoginController::class,'index']);

Route::post('/login',[LoginController::class,'authenticate']);

Route::get('/panti/{panti:slug}', [PantiController::class,'show']);

Route::get('/list-panti', [PantiController::class,'index']);

Route::post('/donasi-uang',[DonasiUangController::class,'store']);

Route::post('/donasi-barang',[DonasiBarangController::class,'store']);

Route::get('/donasi-uang/{donasi_uang:id_donasi}',[DonasiUangController::class,'index']);

Route::get('/donasi-barang/{donasi_barang:id_donasi}',[DonasiBarangController::class,'index']);

Route::get('/about',function(){
    return view('about',['title' => 'About Us']);
});

#Admin

Route::get('/admin/grafik', [AdminController::class,'ubahGrafik'])->middleware('auth');

Route::post('/admin/grafik', [AdminController::class,'updateGrafik'])->middleware('auth');

Route::get('/logout', [LoginController::class,'logout']);

Route::get('/admin/list-donasi-uang',[AdminDonasiUangController::class,'index'])->middleware('auth');

Route::get('/admin/donasi-uang/{donasi_uang:id_donasi}',[AdminDonasiUangController::class,'show'])->middleware('auth');

Route::get('/admin/list-donasi-uang-lunas',[AdminDonasiUangController::class,'indexLunas'])->middleware('auth');

Route::get('/admin/donasi-uang-lunas/{donasi_uang:id_donasi}',[AdminDonasiUangController::class,'showLunas'])->middleware('auth');

Route::post('/admin/donasi-uang/set-status/{donasi_uang:id_donasi}',[AdminDonasiUangController::class,'setStatus'])->middleware('auth');

Route::delete('/admin/donasi-uang/{donasi_uang:id_donasi}',[AdminDonasiUangController::class,'destroy'])->middleware('auth');

Route::get('/admin/list-donasi-barang',[AdminDonasiBarangController::class,'index'])->middleware('auth');

Route::get('/admin/donasi-barang/{donasi_barang:id_donasi}',[AdminDonasiBarangController::class,'show'])->middleware('auth');

Route::get('/admin/list-donasi-barang-lunas',[AdminDonasiBarangController::class,'indexLunas'])->middleware('auth');

Route::get('/admin/donasi-barang-lunas/{donasi_barang:id_donasi}',[AdminDonasiBarangController::class,'showLunas'])->middleware('auth');

Route::post('/admin/donasi-barang/set-status/{donasi_barang:id_donasi}',[AdminDonasiBarangController::class,'setStatus'])->middleware('auth');

Route::delete('/admin/donasi-barang/{donasi_barang:id_donasi}',[AdminDonasiBarangController::class,'destroy'])->middleware('auth');

Route::get('/admin/list-panti',[AdminPantiController::class,'index'])->middleware('auth');

Route::get('/admin/panti/create',[AdminPantiController::class,'create'])->middleware('auth');

Route::post('/admin/panti',[AdminPantiController::class,'store'])->middleware('auth');

Route::get('/admin/panti/{panti:slug}/edit',[AdminPantiController::class,'edit'])->middleware('auth');

Route::post('/admin/panti/{panti:slug}/update',[AdminPantiController::class,'update'])->middleware('auth');

Route::delete('/admin/panti/{panti:slug}',[AdminPantiController::class,'destroy'])->middleware('auth');

Route::get('/admin/panti/checkSlug',[AdminPantiController::class,'checkSlug'])->middleware('auth');

Route::get('/admin/kategori/create',[AdminKategoriKebutuhanController::class,'create'])->middleware('auth');

Route::get('/admin/list-kategori',[AdminKategoriKebutuhanController::class,'index'])->middleware('auth');

Route::post('/admin/kategori',[AdminKategoriKebutuhanController::class,'store'])->middleware('auth');

Route::get('/admin/kategori/{kategori_kebutuhan:nama}/edit',[AdminKategoriKebutuhanController::class,'edit'])->middleware('auth');

Route::post('/admin/kategori/{kategori_kebutuhan:nama}',[AdminKategoriKebutuhanController::class,'update'])->middleware('auth');

Route::delete('/admin/kategori/{kategori_kebutuhan:nama}',[AdminKategoriKebutuhanController::class,'destroy'])->middleware('auth');

Route::get('/admin/kategori/checkSlug',[AdminKategoriKebutuhanController::class,'checkSlug'])->middleware('auth');

Route::get('/admin',[AdminController::class,'index'])->middleware('auth');