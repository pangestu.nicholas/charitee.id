<?php

namespace App\Http\Controllers;
use App\Models\DonasiBarang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\DonasiBarangLunasMail;
use App\Mail\DonasiBarangAdminLunasMail;
use Illuminate\Support\Facades\Mail;

class AdminDonasiBarangController extends Controller
{
    public function index(){
        if(request('keyword')){            
            return view('admin.donasi_barang.index',[
                'all_donasi'=> DonasiBarang::latest()->where('status_donasi',false)->where(request('feature'),'like','%'.request('keyword').'%')->get(),
                'title' => 'Hasil Pencarian '.request('feature').' '.request('keyword')
            ]);
        }
        return view('admin.donasi_barang.index',[
            'all_donasi'=> DonasiBarang::latest()->where('status_donasi',false)->get(),
            'title' => 'List Donasi Barang Belum Lunas'
        ]);
    }

    public function indexLunas(){
        if(request('keyword')){            
            return view('admin.donasi_barang.index_lunas',[
                'all_donasi'=> DonasiBarang::latest()->where('status_donasi',true)->where(request('feature'),'like','%'.request('keyword').'%')->get(),
                'title' => 'Hasil Pencarian '.request('feature').' '.request('keyword')
            ]);
        }
        return view('admin.donasi_barang.index_lunas',[
            'all_donasi'=> DonasiBarang::latest()->where('status_donasi',true)->get(),
            'title' => 'List Donasi Barang Lunas'
        ]);
    }

    public function destroy(DonasiBarang $donasiBarang){
        DB::table('donasi_barangs')->where('id_donasi','=',$donasiBarang->id_donasi)->delete();
        return redirect('/admin/list-donasi-barang');
    }

    public function show(DonasiBarang $donasiBarang){

        return view('admin.donasi_barang.show',[
            'donasi'=> $donasiBarang,
            'title' => 'Donasi Barang '. $donasiBarang->id_donasi
        ]);
    }

    public function showLunas(DonasiBarang $donasiBarang){

        return view('admin.donasi_barang.show_lunas',[
            'donasi'=> $donasiBarang,
            'title' => 'Donasi Barang '. $donasiBarang->id_donasi
        ]);
    }

    public function setStatus(DonasiBarang $donasiBarang){
        DB::table('donasi_barangs')->where('id_donasi',$donasiBarang->id_donasi)->update(['status_donasi'=> true]);
        DB::table('pantis')
            ->where('nama_panti', $donasiBarang->nama_panti)
            ->update(['total_donatur' => DB::raw('total_donatur + 1'),
                    'total_donasi_barang' => DB::raw('total_donasi_barang + 1')]);

        $detail_donasi_barang=[
            'id_donasi'=>$donasiBarang->id_donasi,
            'nama_panti'=> $donasiBarang->nama_panti,
            'email_donatur'=> $donasiBarang->email_donatur,
            'jumlah_uang'=> $donasiBarang->jumlah_uang,
        ];
        try {    
            Mail::to($donasiBarang->email_donatur)->queue(new DonasiBarangLunasMail($detail_donasi_barang));
            Mail::to(env('MAIL_FROM_ADDRESS'))->queue(new DonasiBarangAdminLunasMail($detail_donasi_barang));
        } catch (Exception $ex) {
            return redirect('/admin/list-donasi-barang');
        }
        
        return redirect('/admin/list-donasi-barang');
    }
}
