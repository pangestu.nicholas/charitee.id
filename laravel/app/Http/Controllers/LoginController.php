<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\DonasiBarang;
use App\Models\DonasiUang;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    
    public function home(){
        $jumlah_donasi_uang = DB::table('donasi_uangs')->where('status_donasi',true)->sum('jumlah_uang');
        $total_donatur = 0;
        $total_donatur += DB::table('donasi_uangs')->where('status_donasi',true)->count();
        $total_donatur += DB::table('donasi_barangs')->where('status_donasi',true)->count();
        return view('home',[
            'total_donatur' =>$total_donatur,
            'total_donasi_uang' => number_format($jumlah_donasi_uang,0,',','.'),
            'total_donasi_barang' => DB::table('donasi_barangs')->where('status_donasi',true)->count(),
            'rekomendasi_panti' => DB::table('pantis')->orderBy('total_donatur','desc')->limit(2)->get()
        ]);
    }
    public function index(){
        if (Auth::check()) {
            return redirect()->intended('/admin');
        }
        return view("login.index",[
            "title" => "Login"
        ]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email:dns',
            'password' => 'required'
        ]);
 
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('/admin');
        }
        
    }

    public function logout(){
        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/');
    }
    
    public function sendMail(){
        $detail_donasi_uang=[
            'id_donasi'=>"Tes",
            'nama_panti'=> "Tes",
            'nama_donatur'=> "Tes",
            'asal_departemen' => "Tes",
            'email_donatur'=> "Tes",
            'nomor_kontak_donatur'=> "Tes",
            'jumlah_uang'=> 15000,
            'url' =>  config("APP_URL").'/donasi-uang/'.$validatedData['id_donasi'],
            'url_admin' => config("APP_URL").'/admin/donasi-uang/'.$validatedData['id_donasi'],
        ];
        
        Mail::to('razortear@gmail.com')->queue(new DonasiUangMail($detail_donasi_uang));
        
        return view("login.index",[
            "title" => "Login"
        ]);
    }
}
