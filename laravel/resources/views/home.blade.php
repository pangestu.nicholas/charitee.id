@extends('layouts/main')

@section('container')
    <div id="carouselExampleIndicators" class="carousel slide" style="max-width: 480px; max-height: 125px" data-ride="carousel">
        <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active carousel-homepage">
                <img class="d-block w-100" style="max-width: 480px; max-height: 200px" src="/img/Foto Kegiatan 1.jpg" alt="First slide">
            </div>
            <div class="carousel-item carousel-homepage">
                <img  class="d-block w-100" style="max-width: 480px; max-height: 200px" src="/img/Foto Kegiatan 2.jpg" alt="Second slide">
            </div>
            <div class="carousel-item carousel-homepage">
                <img class="d-block w-100" style="max-width: 480px; max-height: 200px" src="/img/Foto Kegiatan 3.jpg" alt="Third slide">
            </div>
            <div class="carousel-item carousel-homepage">
                <img class="d-block w-100" style="max-width: 480px; max-height: 200px" src="/img/Foto Kegiatan 4.jpg" alt="Fourth slide">
            </div>
            <div class="carousel-item carousel-homepage">
                <img class="d-block w-100" style="max-width: 480px; max-height: 200px" src="/img/Foto Kegiatan 5.jpg" alt="Fifth slide">
            </div>
            <div class="carousel-caption vh-50">
                <p class="px-3 h4">Kegiatan Gradasi Tahun Lalu</p>
            </div>
        </div>
            <a class="carousel-control-prev carousel-control" href="#carouselExampleIndicators" role="button"  data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            </a>
            <a class="carousel-control-next carousel-control" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            </a>
        </div><br><br><br>
    <p class="opacity-75 text-center">Charitee.id memberikan kemudahan berdonasi Uang dan Barang secara instan dan praktis dimanapun anda berada  </p>

    <a href="/list-panti" class="btn btn-lg active mx-auto d-block mb-4" id="btn-mulai-berdonasi" role="button" aria-pressed="true">Mulai Berdonasi</a>
    <center><img class="mb-2" style="max-width:60%" src="{{ asset('/storage/grafik/grafik.jpg') }}" alt="Grafik"></center>
    
    <p class="text-center">Setiap Donasi dari Anda, Adalah Kebahagiaan Anak Yatim</p>
    <h5 class="text-center fw-bold">Rekomendasi Panti</h5>
    <div class='d-flex justify-content-center'>
    @foreach($rekomendasi_panti as $panti)
            @php
                $foto_panti = json_decode($panti->daftar_foto)[0];
            @endphp
            <div class="mb-3 d-inline-block me-2" style="max-width:45%; min-width:45%">
                <div class="card" >
                    <a href="/panti/{{ $panti->slug }}">
                        <img src="{{ asset('/storage/panti-gambars/'.$foto_panti) }}" style="max-width:100%" height="170px"  alt="{{ $panti->nama_panti }}">
                    </a>
                    <div class="card-body" style="max-width:100%">
                        <h5 class="card-title text-center"><a class='text-decoration-none a-panti' href="/panti/{{ $panti->slug }}">{{ $panti->nama_panti }}</a></h5>
                        <p class="card-text small-text text-left a-panti" style="margin-bottom:0px"> Kecamatan {{ $panti->kecamatan }}</p>
                        <p class="card-text small-text text-left a-panti" style="margin-top:0px;;"> Kota {{ $panti->kota }}</p>
                        <p class="small-line-height"> Kebutuhan:</p>
                        @php
                            $list_kategori = json_decode($panti->daftar_kategori_kebutuhan);
                        @endphp
                        @foreach( $list_kategori as $kategori)
                            <td class="d-inline">
                                <img src="{{ asset('/storage/kategori_kebutuhan-ikons/'.$kategori.'.png') }}" height="20" width="20" alt="{{ $kategori }}">
                            </td>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    <div class="row" id="data-donasi-charitee">
        <table>
            <tr>
                {{-- <td><img src="/img/money.png" class="homepage-icon d-block mx-auto mt-3" alt="rupiah"></td> --}}
                <td><img src="/img/money.png" class="homepage-icon d-block mx-auto mt-2" alt="rupiah"></td>
                <td><img src="/img/gift.png" class="homepage-icon d-block mx-auto mt-2" alt="gift"></td>
                <td><img src="/img/users.png" class="homepage-icon d-block mx-auto mt-2" alt="users"></td>
                
            </tr>
            <tr>
                <td class="text-center" style="cell-pading:5px">Total Donasi Uang</td>
                <td class="text-center" style="cell-pading:5px">Total Donasi Barang</td>
                <td class="text-center" style="cell-pading:5px">Total Donatur</td>
            </tr>
            <tr>
                <td class="text-center fw-bold mx-auto pb-2" style="max-width: 160px">Rp {{ $total_donasi_uang }}</td>
                <td class="text-center fw-bold mx-auto pb-2" style="max-width: 160px">{{ $total_donasi_barang }} Donasi</td>
                <td class="text-center fw-bold mx-auto pb-2" style="max-width: 160px">{{ $total_donatur }} Orang</td>
            </tr>
        </table>
    </div>
@endsection