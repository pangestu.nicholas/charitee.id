@extends('layouts/main')

@section('container')
<div class="row justify-content-center mb-4">
    <div class="col-lg-4">
        @if (Auth::check())
            <p>Sudah Login</p>
        @endif
        <main class="form-login">
            <h1 class="h3 mb-3 font-weight-normal text-center">Please login</h1>
            <form action="/login" method="post">
                @csrf
                <div class="d-flex justify-content-center">
                    <input type="email" id="inputEmail" class="mb-2 @error('email') is-invalid @enderror" placeholder="Email address" name="email" value="{{ old('email') }}" required autofocus>
                    @error('email')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="d-flex justify-content-center mb-2">
                    <input type="password" id="inputPassword" class="@error('password') is-invalid @enderror rounded-bottom" placeholder="Password" name="password" required>
                    @error('password')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <button class="w-100 btn btn-lg btn-primary btn-block" type="submit">Login</button>
            </form>
        </main>
    </div>
</div>
@endsection