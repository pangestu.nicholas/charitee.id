@extends('layouts/main')

@section('container')
    <br>
    <p class="small-text"><a class="text-decoration-none" href="/">Home</a> < About Us</p>
    <p>Charitee.id adalah platform yang membantu penyaluran donasi dari masyarakat umum berupa uang maupun barang sesuai kebutuhan Anak-Anak Yatim dari Panti Asuhan mitra kami.</p>
    <p>Secara legal, Platform Charitee.id berada di bawah naungan Fusi Foundation.</p>
    <p>Fusi Foundation adalah organisasi non-profit yang terdaftar pada Kantor KesbangPol Pemkot Depok dan memiliki tugas pokok menyelenggarakan urusan pemerintah daerah berdasarkan asas desentralisasi sesuai dengan Peranturan Walikota Depok nomor 33 tahun 2016 tentang Tupoksi Kantor Kesbangpol Kota Depok.</p>
    <p>Kami juga Bekerjasama dengan Panitia Gema Ramdhan dan Syiar Islami dari Fakultas Teknik UI sebagai Partner kami.</p>
    <p class="fw-bolder">Bagan Organisasi</p>
    <img src="/img/bagan.png" style="max-width:98%; max-height:100%" alt="bagan">
@endsection