@extends('layouts/main')

@section('container')
    <br>
    <p class="small-text"><a class="text-decoration-none" href="/">Home</a> < Detail Donasi Barang</p>
    <div class="text-center">
        <p>ID Donasi Barang: {{ $donasi->id_donasi }}</p>
        <h4>Terimakasih Sudah Berdonasi</h4>
        @if($donasi->status_donasi == true)
            <p>Barang sudah sampai ke panti tujuan</p>
            <p>Jika ada pertanyaan, dapat disampaikan melalui link Whatsapp <a href="https://api.whatsapp.com/send/?phone=6281213434181&?text=Permisi saya ingin menanyakan sesuatu mengenai donasi barang dengan id {{ $donasi->id_donasi }}">berikut</a>.</p>
        @else
            <p>Anda Akan dihubungi via Whatsapp untuk Konfirmasi dan Informasi Ongkir</p>
            <h5>Tahapan Selanjutnya:</h5>
    </div>
            <div class="text-left">
                @if($donasi->metode_pembayaran == "transfer")
                    <p>1) Menunggu admin menghubungi anda perihal biaya ongkir untuk donasi</p>
                    <p>2) Melakukan pembayaran ongkir</p>
                    <p>3) Melakukan konfirmasi pembayaran melalui link Whatsapp dibawah dengan menyertakan Screenshoot dan ID donasi</p>
                    <p>4) Menunggu barang donasi dijemput dan dikirimkan</p>
                    <p>5) Menunggu konfirmasi admin saat barang sudah sampai panti tujuan</p>
                @else
                    <p>1) Menunggu kurir donasi datang</p>
                    <p>2) Membayar biaya pengiriman donasi melalui kurir</p>
                    <p>3) Menunggu konfirmasi admin saat barang sudah sampai panti tujuan</p>
                 @endif
             </div>
            <p class="text-center">Jika ada pertanyaan, dapat disampaikan melalui link Whatsapp <a href="https://api.whatsapp.com/send/?phone=6281213434181&?text=Permisi saya ingin menanyakan sesuatu mengenai donasi barang dengan id {{ $donasi->id_donasi }}">berikut</a>.</p>
        @endif

    
@endsection