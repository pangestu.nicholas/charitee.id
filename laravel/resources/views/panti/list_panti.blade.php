@extends('layouts/main')

@section('container')
<br>
    <p class="small-text"><a class="text-decoration-none" href="/">Home</a> < Pilih Panti</p>
    
    <div>
        <h4>Daftar Panti:</h4><br>
        <div class='justify-content-center align-items-center'>
        @foreach($list_panti as $panti)
            @php
                $foto_panti = json_decode($panti->daftar_foto)[0];
            @endphp
            <div class="mb-3 d-inline-block me-2" style="max-width:45%; min-width:45%">
                <div class="card">
                    <a href="/panti/{{ $panti->slug }}">
                        <center><img src="{{ asset('/storage/panti-gambars/'.$foto_panti) }}" style="max-width:100%" height="170px"  alt="{{ $panti->nama_panti }}"></center>
                    </a>
                    <div class="card-body" style="max-width:100%">
                        <h5 class="card-title text-center"><a class='text-decoration-none a-panti' href="/panti/{{ $panti->slug }}">{{ $panti->nama_panti }}</a></h5>
                        <p class="card-text small-text text-left a-panti" style="margin-bottom:0px"> Kecamatan {{ $panti->kecamatan }} </p>
                        <p class="card-text small-text text-left a-panti" style="margin-top:0px">  Kota {{ $panti->kota }}</p>
                        <p class="small-line-height"> Kebutuhan:</p>
                        @php
                            $list_kategori = json_decode($panti->daftar_kategori_kebutuhan);
                        @endphp
                        @foreach( $list_kategori as $kategori)
                            <td class="d-inline">
                                <img src="{{ asset('/storage/kategori_kebutuhan-ikons/'.$kategori.'.png') }}" height="20" width="20" alt="{{ $kategori }}">
                            </td>
                        @endforeach
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    </div>
    {{ $list_panti->links() }}

@endsection