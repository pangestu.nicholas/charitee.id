<html>
    <head>
        @isset($title)
            <title>Charitee | {{ $title }}</title>
        @else
            <title>Charitee</title>
        @endif
    </head>
    <body>
        <div style="font-size:2vw">
            List Route:<br><br>
            <a href="/" >Home</a><br><br>
            <a href="/admin/grafik">Ubah Grafik</a><br><br>
            <a href="/admin/kategori/create">Buat Kategori Kebutuhan</a><br><br>
            <a href="/admin/panti/create">Buat Panti</a><br><br>
            <a href="/admin/list-donasi-barang">Daftar Donasi Barang (Belum Lunas)</a><br><br>
            <a href="/admin/list-donasi-barang-lunas">Daftar Donasi Barang (Lunas)</a><br><br>
            <a href="/admin/list-donasi-uang">Daftar Donasi Uang (Belum Lunas)</a><br><br>
            <a href="/admin/list-donasi-uang-lunas">Daftar Donasi Uang (Lunas)</a><br><br>
            <a href="/admin/list-kategori">Daftar Kategori Kebutuhan</a><br><br>
            <a href="/admin/list-panti">Daftar Panti</a>
        </div>
    </body>
</html>