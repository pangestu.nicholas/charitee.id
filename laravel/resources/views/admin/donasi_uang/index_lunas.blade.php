<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @isset($title)
      <title>Charitee | {{ $title }}</title>
    @else
      <title>Charitee</title>
    @endif
    <link rel="stylesheet" href="/css/table.css">
  </head>

  <body><p class="small-text"><a class="text-decoration-none" href="/admin">Home Admin</a> < List Donasi Uang (Lunas)</p>
    <h2>List Donasi Uang (Lunas)</h2>
    <form action="/admin/list-donasi-uang-lunas" method="GET">
      <select name="feature" onchange="changePlaceholder()" id="feature">
        <option value="id_donasi" {{ request('feature') == 'id_donasi' ? 'selected':'' }}>ID</option>
        <option value="nama_panti" {{ request('feature') == 'nama_panti' ? 'selected':'' }}>Panti</option>
        <option value="nama_donatur" {{ request('feature') == 'nama_donatur' ? 'selected':'' }}>Nama Donatur</option>
        <option value="asal_departemen" {{ request('feature') == 'asal_departemen' ? 'selected':'' }}>Asal Departemen</option>
      </select>
      <input type="text" id="keyword" name="keyword" placeholder='{{ request("feature") ?? "id_donasi" }}' value= "{{ request('keyword') }}">
      <button type="submit">Cari</button>
    </form>
    <table class="table table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center">No</th>
            <th class="text-center">ID</th>
            <th class="text-center">Panti</th>
            <th class="text-center">Nama Donatur</th>
            <th class="text-center">Asal Departemen</th>
            <th class="text-center">Jumlah Uang</th>
            <th class="text-center">Created At</th>
            <th class="text-center">Action</th>
          </tr>
          @foreach ($all_donasi as $donasi)
              @if($loop->iteration%2 != 0)
                <tr>
              @else
                <tr class="even-row">
              @endif
                  <td class="text-center">{{ $loop->iteration }}</td>
                  <td class="text-center">{{ $donasi->id_donasi }}</td>
                  <td class="text-center">{{ $donasi->nama_panti }}</td>
                  <td class="text-center">{{ $donasi->nama_donatur }}</td>
                  <td class="text-center">{{ $donasi->asal_departemen }}</td>

                  @php
                    $nominal_uang = number_format($donasi->jumlah_uang,0,',','.');
                  @endphp
                  <td class="text-center">Rp {{ $nominal_uang }},-</td>
                  <td class="text-center">{{ $donasi->created_at }}</td>
                  <td class="text-center"> 
                    <a href="/admin/donasi-uang-lunas/{{ $donasi->id_donasi }}" class="badge bg-info"><span data-feather="eye">Detail</span></a>
                  </td>
              </tr>
          @endforeach
        </thead>
        <tbody>
        </tbody>
      </table>
      <script>
        function changePlaceholder(){
          let feature =  document.getElementById('feature').selectedOptions[0].value;
          document.getElementById('keyword').placeholder = feature;
        }
      </script>
  </body>
</html>