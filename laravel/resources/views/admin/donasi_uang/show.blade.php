<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @isset($title)
      <title>Charitee | {{ $title }}</title>
    @else
      <title>Charitee</title>
    @endif
  </head>

  <body>
      <p class="small-text"><a class="text-decoration-none" href="/admin">Home Admin</a> < <a class="text-decoration-none" href="/admin/list-donasi-uang">List Donasi Uang (Belum Lunas)</a> < Donasi Uang {{ $donasi->id_donasi }}</p>
      <p>ID: {{ $donasi->id_donasi }}</p>
      <p>Tujuan Panti: {{ $donasi->nama_panti }}</p>
      <p>Nama Donatur: {{ $donasi->nama_donatur }}</p>
      <p>Asal Departemen: {{ $donasi->asal_departemen }}</p>
      <p>Email Donatur: {{ $donasi->email_donatur }}</p>
      <p>Kontak Donatur: {{ $donasi->nomor_kontak_donatur }}</p>
      @php
        $nominal_uang = number_format($donasi->jumlah_uang,0,',','.');
      @endphp
      <p>Nominal Uang: Rp {{ $nominal_uang }},-</p>
      <form action="/admin/donasi-uang/set-status/{{ $donasi->id_donasi }}" method="POST">
        @csrf
          <button type="submit" onclick="return confirm('are you sure update this?')">Change Status</button>
      </form>
  </body>
</html>