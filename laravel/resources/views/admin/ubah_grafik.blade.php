<html>
    <head>
        @isset($title)
            <title>Charitee | {{ $title }}</title>
        @else
            <title>Charitee</title>
        @endif
    </head>
    <body>
        <p class="small-text"><a class="text-decoration-none" href="/admin">Home Admin</a> < Ubah Grafik</p>
         <h1 class="h2">Ubah Grafik</h1>
         <form method="POST" action="/admin/grafik" enctype="multipart/form-data">
        @csrf
         <div class="mb-3">
            <label class="form-label" for="grafik">Grafik</label>            
            <img class="img-fluid img-preview mb-3 col-sm-5" width="100px" height="100px"><br>
            <input type="file" onchange="previewImage()" class="form-control @error('grafik') is-invalid @enderror" id="grafik" name="grafik">
            @error('grafik')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
        </div><br>
         <button type="submit" class="btn btn-primary">Ubah Grafik</button>
    </form>
    </body>
    <script>

    function previewImage(){
        const image = document.querySelector('#grafik');
        const imagePreview = document.querySelector('.img-preview');

        imagePreview.style.display = 'block';

        const oFReader= new FileReader();
        oFReader.readAsDataURL(image.files[0]);

        oFReader.onload = function(oFREvent){
            imagePreview.src = oFREvent.target.result;
        }
    }
</script>
</html>