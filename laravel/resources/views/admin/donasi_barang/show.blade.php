<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @isset($title)
      <title>Charitee | {{ $title }}</title>
    @else
      <title>Charitee</title>
    @endif
  </head>

  <body>
    <p class="small-text"><a class="text-decoration-none" href="/admin">Home Admin</a> < <a class="text-decoration-none" href="/admin/list-donasi-barang">List Donasi Barang (Belum Lunas)</a> < Donasi Barang {{ $donasi->id_donasi }}</p>
      <p>ID: {{ $donasi->id_donasi }}</p>
      <p>Tujuan Panti: {{ $donasi->nama_panti }}</p>
      <p>Nama Donatur: {{ $donasi->nama_donatur }}</p>
      <p>Asal Departemen: {{ $donasi->asal_departemen }}</p>
      <p>Email Donatur: {{ $donasi->email_donatur }}</p>
      <p>Kontak Donatur: {{ $donasi->nomor_kontak_donatur }}</p>
      <p>Alamat Barang: {{ $donasi->alamat_barang }}</p>
      <p>Keterangan Barang: {{ $donasi->keterangan_barang }}</p>
      <p>Berat Barang: {{ $donasi->berat_barang }} kg</p>
      <p>Metode Pembayaran: {{ $donasi->metode_pembayaran }}</p>
      <form action="/admin/donasi-barang/set-status/{{ $donasi->id_donasi }}" method="POST">
        @csrf
          <button type="submit" onclick="return confirm('are you sure to update this?')">Change Status</button>
      </form>
  </body>
</html>