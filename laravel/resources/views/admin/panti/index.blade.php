<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @isset($title)
      <title>Charitee | {{ $title }}</title>
    @else
      <title>Charitee</title>
    @endif
    <link rel="stylesheet" href="/css/table.css">
  </head>

  <body>
    <p class="small-text"><a class="text-decoration-none" href="/admin">Home Admin</a> < List Panti</p>
    <h2>List Panti</h2>
    <div style="display: flex">
      <a href="/admin/panti/create" style="margin-right: 1vw">Tambah Panti</a>
      <form action="/admin/list-panti" method="GET">
        <select name="feature" onchange="changePlaceholder()" id="feature">
          <option value="nama_panti" {{ request('feature') == 'nama_panti' ? 'selected':'' }}>Nama</option>
          <option value="kota" {{ request('feature') == 'kota' ? 'selected':'' }}>Kota</option>
          <option value="kecamatan" {{ request('feature') == 'kecamatan' ? 'selected':'' }}>Kecamatan</option>
        </select>
        <input type="text" id="keyword" name="keyword" placeholder='{{ request("feature") ?? "nama_panti" }}' value= "{{ request('keyword') }}">
        <button type="submit">Cari</button>
      </form>
    </div>

    <table>
        <thead>
          <tr>
            <th class="text-center">No</th>
            <th class="text-center">Nama</th>
            <th class="text-center">Kecamatan</th>
            <th class="text-center">Kota</th>
            <th class="text-center">Kontak</th>
            <th class="text-center">Jumlah Uang</th>
            <th class="text-center" style="border: solid 1px black">Action</th>
          </tr>
          @foreach ($all_panti as $panti)
          @if($loop->iteration%2 != 0)
            <tr>
          @else
            <tr class="even-row">
          @endif
                  <td class="text-center">{{ $loop->iteration }}</td>
                  <td class="text-center">{{ $panti->nama_panti }}</td>
                  <td class="text-center">{{ $panti->kecamatan }}</td>
                  <td class="text-center">{{ $panti->kota }}</td>
                  @php
                    $jumlah_uang = number_format($panti->jumlah_uang,0,',','.');
                  @endphp
                  <td class="text-center">{{ $panti->nomor_kontak }}</td>
                  <td class="text-center">Rp {{  $jumlah_uang }},-</td>
                  <td class="text-center"> 
                    <a href="/panti/{{ $panti->slug }}" class="badge bg-info"><span data-feather="eye">Detail</span></a>
                    <a href="/admin/panti/{{ $panti->slug }}/edit" class="badge bg-info"><span data-feather="eye">Edit</span></a>
                    <form action="/admin/panti/{{ $panti->slug }}" class="d-inline" method="POST">
                        @method('delete')
                        @csrf
                        <button type="submit" class="badge bg-danger border-0" id="btn-delete-post" onclick="return confirm('are you sure?')"><span data-feather="x-circle"></span>Delete</button>
                    </form>
                  </td>
              </tr>
          @endforeach
        </thead>
        <tbody>
        </tbody>
      </table>
      <script>
        function changePlaceholder(){
          let feature =  document.getElementById('feature').selectedOptions[0].value;
          document.getElementById('keyword').placeholder = feature;
        }
      </script>
  </body>
</html>