@component('mail::message')
    @php
        $nominal_uang = number_format($detail_donasi_uang['jumlah_uang'],0,',','.');
        $tanggal_donasi = date('d-m-Y');
    @endphp
    <div style="color: #000000">
        <h1 style="text-align: center">ID Donasi:</h1>
        <h1 style="text-align: center">{{ $detail_donasi_uang['id_donasi'] }}</h1>
        <p class="fw-bolder"></p>
        @component('mail::table')
        |    Informasi Donasi                       |                                                        |                                                   |
        |:-----------------------------------------:| :----------------------------------------------------: |:------------------------------------------------: |
        | **Nama**                                  | **Asal Departemen**                                    |  **Kontak**                                       |
        | {{ $detail_donasi_uang['nama_donatur'] }} | {{ $detail_donasi_uang['asal_departemen'] }}           | {{ $detail_donasi_uang['nomor_kontak_donatur'] }} |
        | **Tujuan Panti**                          | **Nominal Uang**                                     | **Tanggal Donasi**                                  |
        | {{ $detail_donasi_uang['nama_panti'] }}   | Rp {{ $nominal_uang }},-                                | {{  $tanggal_donasi  }}                          |
        @endcomponent
        <hr>
    <h2 style="margin-bottom:1px">Harap segera membayarkan nominal uang ke rekening berikut:</h2>
    <h1 style="text-align: center; margin-bottom:0px">9975720150000002</h1>
    </div>
    @component('mail::button', ['url' => $detail_donasi_uang['url'], 'color' => 'success'])
    Detail Donasi
    @endcomponent
@endcomponent