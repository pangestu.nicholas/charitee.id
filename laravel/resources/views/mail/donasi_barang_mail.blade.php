@component('mail::message')
    @php
        $tanggal_donasi = date('d-m-Y');
    @endphp
    <div style="color: #000000">
        <h1 style="text-align: center">ID Donasi:</h1>
        <h1 style="text-align: center">{{ $detail_donasi_barang['id_donasi'] }}</h1>
        <p class="fw-bolder"></p>
        @component('mail::table')
        |    Informasi Donasi                         |                                                        |                                                     |
        |:-------------------------------------------:| :----------------------------------------------------: |:------------------------------------------------:   |
        | **Nama**                                    | **Asal Departemen**                                    |  **Kontak**                                         |
        | {{ $detail_donasi_barang['nama_donatur'] }} | {{ $detail_donasi_barang['asal_departemen'] }}         | {{ $detail_donasi_barang['nomor_kontak_donatur'] }} |
        | **Tujuan Panti**                            | **Tanggal Donasi**                                     | **Metode Pembayaran**                                                    |
        | {{ $detail_donasi_barang['nama_panti'] }}   | {{  $tanggal_donasi  }}                                | {{  $detail_donasi_barang['metode_pembayaran']  }}                                                    |
        @endcomponent
        <hr>
        <p>Alamat Barang: {{ $detail_donasi_barang['alamat_barang'] }},-</p>
        <p>Keterangan Barang: {{ $detail_donasi_barang['keterangan_barang'] }},-</p>
        <p>Berat Barang: {{ $detail_donasi_barang['berat_barang'] }}kg</p>
        <hr>
        <p>Silahkan menunggu informasi tarif ongkir dari Admin Charitee.id </p>
    </div>
    @component('mail::button', ['url' => $detail_donasi_barang['url'], 'color' => 'success'])
    Detail Donasi
    @endcomponent
@endcomponent