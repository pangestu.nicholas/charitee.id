@component('mail::message')
    <h3>Detail Donasi</h3>
    <p>Donasi barang atas nama {{ $detail_donasi_barang['nama_donatur'] }} dari {{ $detail_donasi_barang['asal_departemen'] }} untuk panti {{ $detail_donasi_barang['nama_panti'] }} dengan {{ $detail_donasi_barang['metode_pembayaran'] }}</p>
    <p>Nomor Kontak: {{ $detail_donasi_barang['nomor_kontak_donatur'] }}</p>
    @component('mail::button', ['url' => $detail_donasi_barang['url_admin']])
    Halaman Donasi
    @endcomponent
@endcomponent