<div class="footer-basic text-center">
    <footer>
        <div class="social">
            <a href="https://www.facebook.com/Chariteeid-109296365083386"><i class="icon fa fa-brands fa-facebook"></i>
            <a href="http://instagram.com/charitee_id"><i class="icon fa fa-brands fa-instagram"></i></a>
        </div>
        <p class="copyright fw-bold mb-2">Partner Kami</p>
        <ul class="list-inline">
            <li class="list-inline-item"><a href="https://eng.ui.ac.id/">Fakultas Teknik UI</a></li>
            <li class="list-inline-item"><a href="https://www.instagram.com/fusiftui/?hl=id">Fusi FTUI</a></li>
            <li class="list-inline-item"><a href="https://www.instagram.com/fusi.foundation/?hl=id">Fusi Foundation</a></li>
        </ul>
        <p class="copyright fw-bold">©2022 Charitee.id </p>
    </footer>
</div>
