<nav class="justify-content-center mx-auto" style="max-width: 480px;">
    <div class="cancel-icon-menu"><span class="fa fa-times"></span></span></div>        
    <div class="menu-icon"><span class="fa fa-bars"></span></span></div>
    <div class="logo">
        <a href="/"><img src="/img/logo.png" width="45px" height="45px" alt="logo"></a>
        <a href="/"><p class="fw-bolder text-white fs-4 d-inline-block" style="top: 14px; position: relative;" id="logo-name">Charitee.id</p></a>
    </div>
    <div class="nav-items">
        <li><a href="/">Home</a></li>
        <li><a href="/about">About Us</a></li>
        <li><a href="/login">Login Admin</a></li>
    </div>
    <div class="search-icon"><span class="fa fa-search"></span></span></div>
    <div class="cancel-icon"><span class="fa fa-times"></span></span></div>
    <form action="/list-panti" method="GET">
        <input type="text" name="nama_panti" class="search-data" placeholder="Cari Nama Panti" value= "{{ request('nama_panti') }}">
        <button type="submit" class="btn_primary"><i class="fa fa-search" id="search-icon"></i></button>
    </form>
</nav>    