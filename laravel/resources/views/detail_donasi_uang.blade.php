@extends('layouts/main')

@section('container')
<br>
<p class="small-text"><a class="text-decoration-none" href="/">Home</a> < Detail Donasi Uang</p>
<div class="text-center">
    <p>ID Donasi Uang: {{ $donasi->id_donasi }}</p>
    @php
        $nominal_uang = number_format($donasi->jumlah_uang,0,',','.');
    @endphp
    
    <h4>Total Donasi anda sebesar:</h4>
    
    <h4>Rp {{ $nominal_uang }},-</h4>
    @if($donasi->status_donasi == true)
        <p>Donasi anda telah LUNAS</p>
        <h4>Terima Kasih Telah Berdonasi</h4>
        <p>Jika ada pertanyaan, dapat disampaikan melalui link Whatsapp <a href="https://api.whatsapp.com/send/?phone=6281213434181&?text=Permisi saya ingin menanyakan sesuatu mengenai donasi uang dengan id {{ $donasi->id_donasi }}">berikut</a>.</p>
    @else
    <p>Silahkan Lakukan Pembayaran via Rekening Berikut</p>
    <h4>1270009822956</h4>
    <h4>Atas Nama Fusi Talenta Unggul</h4>
    <p>Silahkan Konfirmasi Pembayaran Anda via Link Whatsapp <a href="https://api.whatsapp.com/send/?phone=6281213434181&?text=Permisi saya ingin menanyakan sesuatu mengenai donasi uang dengan id {{ $donasi->id_donasi }}">ini</a> </p>
    <h5>Tahapan Selanjutnya:</h5>
    </div>
    <div class="text-left">
    <p>1) Melakukan Pembayaran ke Rekening Fusi Talenta Unggul</p>
    <p>2) Melakukan Konfirmasi pembayaran melalui link Whatsapp diatas dengan menyertakan Screenshoot dan ID donasi</p>
    <p>3) Admin akan memvalidasi pembayaran yang telah dilakukan</p>
    </div>
    @endif
    
    <p class="text-center">Jika ada pertanyaan, dapat disampaikan melalui link Whatsapp diatas.</p>
    

@endsection